'use strict';

window.addEventListener('DOMContentLoaded',()=>{
//Tabs
    let tabs=document.querySelectorAll('.tabheader__item'),
        tabsContent=document.querySelectorAll('.tabcontent'),
        tabsParent=document.querySelector('.tabheader__items');       
    
    function hideTabContent(){
        tabsContent.forEach((item)=>{
            //item.style.display="none";
            item.classList.add('hide');
            item.classList.remove('show','fade');
        });
        tabs.forEach(item => {
            item.classList.remove('tabheader__item_active');
        });
	}
//в стандарте ES6 появилась возможность подставлять параметры по умолчанию
    function showTabContent(i=0){  
       // tabsContent[i].style.display="block";
        tabsContent[i].classList.add('show','fade');
        tabsContent[i].classList.remove('hide');
        tabs[i].classList.add('tabheader__item_active');
    }

    hideTabContent();
    showTabContent();

    tabsParent.addEventListener('click', function(event) {
		const target = event.target;
		if(target && target.classList.contains('tabheader__item')) {
            tabs.forEach((item, i) => {
                if (target == item) {
                    hideTabContent();
                    showTabContent(i);
                }
            });
		}
    });

//Timer
    const deadline='2021-02-08'; //строковое, т.к либо менеджер ввел в инпуте, либо база данных
    getTimeRemaining(deadline);
    setClock('.timer',deadline);

    function getTimeRemaining(endtime){
        const t=(Date.parse(endtime)-Date.parse(new Date()))/1000,
        daysValue=~~(t/86400),
        hoursValue=~~(t%86400/3600),
        minutesValue=~~(t%86400%3600/60),
        secondsValue=~~(t%86400%3600%60);
        //проще часы=t/60*60%24 минуты=t/60%60 секунды=t%60 

        return {
            'total':t,
            daysValue,
            hoursValue,
            minutesValue,
            secondsValue
        };
    }
    
        function setClock(selector,endtime){
        const timer=document.querySelector(selector),
            days=document.querySelector('#days'),
            hours=timer.querySelector('#hours'),
            minutes=timer.querySelector('#minutes'),
            seconds=timer.querySelector('#seconds'),
            timeInterval=setInterval(updateClock,1000);

            updateClock();

            function updateClock(){
                
                const t=getTimeRemaining(endtime);
                if(t.total<=0){
                    clearInterval(timeInterval);
                } else{
                days.innerHTML=t.daysValue;
                hours.innerHTML=getZero(t.hoursValue);
                minutes.innerHTML=getZero(t.minutesValue);
                seconds.innerHTML=getZero(t.secondsValue);
                }
               
            }
            
            function getZero(num){
                if(num>=0 && num<10){
                    return `0${num}`;
                } else {
                    return num;
                }
            }
           
           
    }

//Modal
    const modalTrigger=document.querySelectorAll('[data-modal]'),
          modal=document.querySelector('.modal'),
          modalCloseBtn=document.querySelector('[data-close]');

    modalTrigger.forEach((btn)=>{
        btn.addEventListener('click',showModal);
    });  
    window.addEventListener('scroll',showModalByScrollEnd); //,{once:true} не подходит, т.к после первого скрола сё
    const modalTimerId=setTimeout(showModal,10000);



    modalCloseBtn.addEventListener('click', hideModal);

    modal.addEventListener('click',(e)=>{
        if(e.target===modal){
            hideModal();
        }
    });
    document.addEventListener('keydown',(e)=>{
        if(e.code=='Escape' && modal.classList.contains('show')){
            hideModal();
        }
    });



    function showModal(){
        modal.classList.add('show');
        modal.classList.remove('hide');
        document.body.style.overflow='hidden';
        clearTimeout(modalTimerId);
    }
    function showModalByScrollEnd(){ 
        if(document.documentElement.scrollTop+document.documentElement.clientHeight>=
            document.documentElement.scrollHeight){ //document.documentElement.scrollTop ~ window.pageYOffset
                showModal();
                window.removeEventListener('scroll',showModalByScrollEnd);
        }
    }
    function hideModal(){
        modal.classList.remove('show');
        modal.classList.add('hide'); 
        document.body.style.overflow=''; 
    }

    //Class menu item
    class MenuCard{
            constructor(src,alt,title,descr,price,parentselector, ...classes){
                this.src=src;
                this.alt=alt;
                this.title=title;
                this.descr=descr;
                this.price=price;
                this.transfer=27;
                this.parent=document.querySelector(parentselector);
                this.classes=classes;
            }
        changeToUAH(){
            this.price=this.price *this.transfer;
        }
        render(){
            const element=document.createElement('div');
            if(this.classes.length==0){
                this.element='menu__item';
                element.classList.add(this.element);
            } else{
                this.classes.forEach(className=>element.classList.add(className));
            }
                element.innerHTML=`
                        <img src=${this.src} alt=${this.alt}>
                        <h3 class="menu__item-subtitle">${this.title}</h3>
                        <div class="menu__item-descr">${this.descr}</div>
                        <div class="menu__item-divider"></div>
                        <div class="menu__item-price">
                            <div class="menu__item-cost">Цена:</div>
                            <div class="menu__item-total"><span>${this.price}</span> грн/день</div>
                        </div>
                `;
                this.parent.append(element);

        }
    }
    new MenuCard(
        'img/tabs/vegy.jpg',
        'vegy',
        'Меню "Фитнес"',
        'Меню "Фитнес" - это новый подход к приготовлению блюд: больше свежих овощей и фруктов. Продукт активных и здоровых людей. Это абсолютно новый продукт с оптимальной ценой и высоким качеством!',
         229,
         '.menu .container',
         'menu__item',
         'big'
        ).render(); //сделает и исчезнет
        
    new MenuCard(
        'img/tabs/elite.jpg',
        'elite',
        'Меню “Премиум”',
        'В меню “Премиум” мы используем не только красивый дизайн упаковки, но и качественное исполнение блюд. Красная рыба, морепродукты, фрукты - ресторанное меню без похода в ресторан!',
         550,
        '.menu .container',
        //'menu__item'
        ).render(); //сделает и исчезнет

    new MenuCard(
    'img/tabs/post.jpg',
    'post',
    'Меню "Постное"',
    'Меню “Постное” - это тщательный подбор ингредиентов: полное отсутствие продуктов животного происхождения, молоко из миндаля, овса, кокоса или гречки, правильное количество белков за счет тофу и импортных вегетарианских стейков.',
     430,
    '.menu .container',
    'menu__item'
    ).render(); //сделает и исчезнет


    //отправка данных с форм на сервер
    const forms=document.querySelectorAll('form');
    const message={
       loading:'Загрузка',
       success:'Спасибо! Скоро мы с вами свяжемся',
       failed:'Что-то пошло не так..'
    };

    forms.forEach(item=>{
        postData(item);
    });

    function postData(form){
        form.addEventListener('submit',(e)=>{//попытка отправить форму
            e.preventDefault();

            const statusMessage=document.createElement('div');
            statusMessage.classList.add('status');
            statusMessage.textContent=message.loading;
            form.append(statusMessage);



        const formData=new FormData(form); //собираем данные из формы, благодаря атрибуту name
        
        const object={};
        formData.forEach((value,key)=>{
        object[key]=value;
        });

        fetch('server1.php',{
            method:"POST",
            headers:{ //нужно для JSON
            'Content-type':'application/json'
            },
            body:JSON.stringify(object) //formData
        })
        .then((data)=>data.text()) 
        .then(data => {
                console.log(data);
                statusMessage.textContent=message.success;
                setTimeout(()=>{
                 statusMessage.remove();   
                },2000);
            })
            .catch(()=>{
                statusMessage.textContent=message.failed;
            })
            .finally(()=>{
                form.reset();
            });

            //const request= new XMLHttpRequest();
            // request.open('POST','server.php');
            // request.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            //заголовок в POST не нужен если данные передаются такими какие они есть
            //const formData=new FormData(form); //собираем данные из формы, благодаря атрибуту name

            //formDAte это не простой объект, поэтому создадим копию
            // const object={};
            // formData.forEach((value,key)=>{
            // object[key]=value;
            // });

            //const json=JSON.stringify(object);

            // request.send(json);

            // request.addEventListener('load',()=>{
            //     if(request.status==200){
            //         console.log(request.response);
            //         statusMessage.textContent=message.success;
            //         form.reset();
            //         setTimeout(()=>{
            //          statusMessage.remove();   
            //         },2000);
            //     } else{
            //         statusMessage.textContent=message.failed;
            //     }
            // });
        });
    }

    const prevModalDialog=document.querySelector('.modal__dialog');
    function showThanksModal(){
        prevModalDialog.classList.add('hide');
        showModal(); 
    }
    
});

    